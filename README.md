# Hatchd Django template

Cookiecutter template for creating a new Django API featuring:

- package version management using Poetry
- initial project and app setup
- custom user model
- healthcheck endpoints for both nagios and an AWS ELB
- Sentry error reporting and graylog logging


## Requirements

You will need to install [cookiecutter](https://cookiecutter.readthedocs.io/en/latest/installation.html):

For the easiest way to install it, consider using [pipx](https://github.com/pipxproject/pipx#install-pipx):

    brew install pipx
    pipx ensurepath

You can then install cookiecutter:

    pipx install cookiecutter


In addition, you will need to install [poetry](https://python-poetry.org/docs/master/#installing-with-the-official-installer), install only using the official script or pipx:

    curl -sSL https://install.python-poetry.org | python3 -
OR

    pipx install poetry


## Usage

To create a new project with this template:

    cookiecutter /path/to/this/repo

You can also simply use the bitbucket repo directly:

    cookiecutter https://bitbucket.org/hatchd/django-template-hatchd.git


## Update Package Version

### Python/Django

1. Update the `cookiecutter.json` version number, it will automatically populate the new version number in `pyproject.toml`.

### Others

1. Create a new project using this template.
2. Show all package that needed update:

   ```
   poetry show -o
   ```
3. Update all listed package:

   ```
   poetry add FOO_PACKAGE@latest FOOFOO_PACKAGE@latest
   ```
   AND/OR
   ```
   poetry add --group=dev FOO@latest FOOFOO@latest
   ```
4. Run poetry update to ensure package dependencies are also up-to-date:

   ```
   poetry update
   ```
5. Compare two `pyproject.toml` with the template and update accordingly.
