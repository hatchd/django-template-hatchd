import os
import secrets
import shutil
import subprocess
from pathlib import Path
from string import Template

print("Generating secret keys")
DJANGO_SETTINGS_FOLDER = Path("src/{{cookiecutter.project_name}}/settings")

for file in DJANGO_SETTINGS_FOLDER.iterdir():
    # skip the empty __init__ file
    if file.name != "__init__.py":
        with file.open("r+") as settings_file:
            template = Template(settings_file.read())
            settings_text = template.substitute(SECRET_KEY_REPLACE_ME=secrets.token_urlsafe(50))
            settings_file.seek(0)
            settings_file.write(settings_text)
            print(f" updated {file.name}")


if shutil.which("pyenv") is not None:
    print("Installing python version")
    subprocess.run(["pyenv", "install", "--skip-existing"], check=True)
else:
    # Mainly for within bitbucket pipeline, where pyenv isn't available
    print("pyenv not found locally, skipping python version install")

print("Creating virtual env and installing dependencies")
subprocess.run(["poetry", "install"], check=True)

if os.environ.get("CI", None) is None:
    print("Initialize a git repository")
    subprocess.run(["git", "init"], check=True)

    print("Creating local database '{{cookiecutter.project_folder}}-dev'")
    subprocess.run(["createdb", "{{cookiecutter.project_folder}}-dev"], check=True)

    print("Create migration files")
    subprocess.run(["poetry", "run", "python", "src/manage.py", "makemigrations", "--no-input"], check=True)

    print("Running pending migrations")
    subprocess.run(["poetry", "run", "python", "src/manage.py", "migrate", "--no-input"], check=True)

    print("Running pre-commit setup")
    subprocess.run(["poetry", "run", "pre-commit", "install"])
else:
    print("Skipping database creation and migrations within CI environment")

print("New project successfully generated in {{cookiecutter.project_folder}}")
