import threading


class GlobalRequestMiddleware:
    # data that is scoped to the current thread
    _data = threading.local()

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # store the request before view processing
        self._data.request = request

        response = self.get_response(request)

        # clean up the request afterwards
        try:
            del self._data.request
        except AttributeError:
            pass

        return response

    @classmethod
    def get_request(cls):
        try:
            request = cls._data.request
        except AttributeError:
            request = None

        return request
