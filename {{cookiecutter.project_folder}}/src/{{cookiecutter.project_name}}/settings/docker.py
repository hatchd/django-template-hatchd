""" Settings file for local docker
"""

from .base import *  # noqa

SECRET_KEY = "${SECRET_KEY_REPLACE_ME}"
DEBUG = True

# Use the host computers postgres for the database
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "{{cookiecutter.project_folder}}-dev",
        "USER": "postgres",
        "PASSWORD": "",
        "HOST": "host.docker.internal",
        "PORT": "5432",
    }
}

SHOW_API_DOCS = True
