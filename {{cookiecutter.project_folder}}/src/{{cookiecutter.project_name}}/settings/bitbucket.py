""" Settings file for bitbucket pipelines
"""

from .base import *  # noqa

SECRET_KEY = "${SECRET_KEY_REPLACE_ME}"
DEBUG = False

# Use the pipeline database
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "django",
        "USER": "django",
        "PASSWORD": "django",
        "HOST": "127.0.0.1",
        "PORT": "5432",
    }
}

SHOW_API_DOCS = True
