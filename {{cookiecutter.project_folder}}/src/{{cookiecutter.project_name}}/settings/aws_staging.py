""" Settings file for Staging stack deployed on AWS
"""
from .base import *  # noqa

SECRET_KEY = "${SECRET_KEY_REPLACE_ME}"

GRAYLOG_ENABLED = True
SENTRY_DNS = ""

# Database settings
DATABASES = {}
