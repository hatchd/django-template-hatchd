""" Settings file for local development
"""
from .base import *  # noqa

SECRET_KEY = "${SECRET_KEY_REPLACE_ME}"
DEBUG = True

SHOW_API_DOCS = True
