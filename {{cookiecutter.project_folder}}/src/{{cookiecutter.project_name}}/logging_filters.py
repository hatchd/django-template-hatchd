from logging import Filter

from django.conf import settings

from {{cookiecutter.project_name}}.middleware import GlobalRequestMiddleware


class RequestContextFilter(Filter):
    def filter(self, record):
        # check if record already has the request object, otherwise grab it from the middleware
        request = getattr(record, "request", GlobalRequestMiddleware.get_request())

        if request:
            record.http_host = request.get_host()
            record.http_url = request.get_raw_uri()
            record.http_cookies = "; ".join([f"{key}={val}" for key, val in request.COOKIES.items()])
            record.http_remote_addr = request.META.get("REMOTE_ADDR", "")
            record.http_user_agent = request.META.get("HTTP_USER_AGENT", "")

        # Capture the environment we're on by parsing the settings module
        # e.g. "ecs_dev" from "{{cookiecutter.project_name}}.settings.ecs_dev"
        record.environment = settings.SETTINGS_MODULE.split(".")[-1]

        return True


class RequireGraylogEnabledTrue(Filter):
    def filter(self, record):
        return settings.GRAYLOG_ENABLED
