from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext_lazy as _

from .forms import UserChangeForm, UserCreationForm
from .models import User


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {"fields": ["email", "name", "password"]}),
        (
            _("Permissions"),
            {
                "fields": ["is_active", "is_staff", "is_superuser", "groups", "user_permissions"],
            },
        ),
        (_("Important dates"), {"fields": ["last_login", "date_joined"]}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "name", "password1", "password2"),
            },
        ),
    )

    # Forms for add / edit platform users
    form = UserChangeForm
    add_form = UserCreationForm

    # List display details
    list_display = ("email", "name", "is_staff", "last_login")
    list_filter = ("is_staff", "is_superuser", "is_active", "groups")
    search_fields = ("email", "name")
    ordering = ("email",)
    filter_horizontal = ("groups", "user_permissions")

    def get_readonly_fields(self, request, obj=None):
        return ["last_login", "date_joined", "is_staff", "is_superuser"]
