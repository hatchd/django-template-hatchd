from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _


class UserManager(BaseUserManager):
    """
    Custom User manager that uses the email address as unique identifiers for users
    """

    def _create_user(self, email, name, password, **extra_fields):
        """
        Creates and saves a new User with the given email, name and password.
        Additionally passes through extra_fields to the model init
        """
        if not email:
            raise ValueError("A users email must be set")

        email = self.normalize_email(email)
        user = self.model(email=email, name=name, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, name, password):
        """Creates and saves a superuser, that can login to the django admin and has all permissions implicitly"""
        return self._create_user(
            email=email,
            name=name,
            password=password,
            is_active=True,
            is_staff=True,
            is_superuser=True,
        )

    def create_user(self, email, name, password):
        """Creates a user which cannot log into the django admin"""
        return self._create_user(
            email=email,
            name=name,
            password=password,
            is_active=True,
            is_staff=False,
            is_superuser=False,
        )


class User(AbstractBaseUser, PermissionsMixin):
    """Default user for {{cookiecutter.project_name}}."""

    email = models.EmailField(
        _("email"),
        help_text=_("Email address for the user."),
        max_length=150,
        unique=True,
    )
    name = models.CharField(
        _("name"),
        help_text=_("Full name of the user."),
        max_length=150,
    )
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), auto_now_add=True)

    objects = UserManager()

    USERNAME_FIELD = "email"
    EMAIL_FIELD = "email"
    REQUIRED_FIELDS = ["name"]

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return self.email
