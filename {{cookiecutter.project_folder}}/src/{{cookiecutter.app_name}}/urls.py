from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from {{cookiecutter.app_name}} import views

app_name = "{{cookiecutter.app_name}}"

urlpatterns = [
    path("healthcheck", views.NagiosHealthcheckView.as_view(), name="nagios-healthcheck"),
    path("healthcheck/elb", views.ELBHealthcheckView.as_view(), name="elb-healthcheck"),
    path("auth/token", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("auth/token/refresh", TokenRefreshView.as_view(), name="token_refresh"),
]
