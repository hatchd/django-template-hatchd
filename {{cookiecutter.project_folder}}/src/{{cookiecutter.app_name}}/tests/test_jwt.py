import pytest
from django.urls import reverse


@pytest.mark.django_db
class TestJWT:
    def test_jwt_tokens(self, user, client):
        response = client.post(
            path=reverse("app:token_obtain_pair"), data={"email": user.email, "password": "Pass123#"}
        )
        assert response.status_code == 200

        data = response.json()
        assert data.get("access") is not None
        assert data.get("refresh") is not None

        response = client.post(path=reverse("app:token_refresh"), data={"refresh": data.get("refresh")})
        assert response.status_code == 200

        data = response.json()
        assert data.get("access") is not None
        assert not data.get("refresh")
