import os

import pytest
from django.urls import reverse
from rest_framework import status

from {{cookiecutter.app_name}}.utils.healthcheck import Healthcheck


@pytest.mark.django_db
class TestHealthcheck:
    def test_nagios_healthcheck(self, client):
        response = client.get(reverse("{{cookiecutter.app_name}}:nagios-healthcheck"))
        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert data["status"] == Healthcheck.STATUS_OK
        assert "unknown" in data["release"]

        # try setting environment variables now
        os.environ["release_version"] = "test version 1.0"
        os.environ["build_date"] = "Test today"

        response = client.get(reverse("{{cookiecutter.app_name}}:nagios-healthcheck"))
        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert data["status"] == Healthcheck.STATUS_OK
        assert "test version 1.0" in data["release"]
        assert "Test today" in data["release"]

    def test_elb_healthcheck(self, client, django_user_model):
        response = client.get(reverse("{{cookiecutter.app_name}}:elb-healthcheck"))
        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert data["users"] == 0

        # create a user in the database
        django_user_model.objects.create_user("test@example.com", "Mr Test", None)

        response = client.get(reverse("{{cookiecutter.app_name}}:elb-healthcheck"))
        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert data["users"] == 1
