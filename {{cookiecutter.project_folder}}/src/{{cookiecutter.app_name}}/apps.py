import os

import sentry_sdk
from django.apps import AppConfig
from django.conf import settings
from sentry_sdk.integrations.django import DjangoIntegration


class {{ cookiecutter.app_name.title().replace("_", "") }}Config(AppConfig):
    name = "{{cookiecutter.app_name}}"
    verbose_name = "{{ cookiecutter.app_name.title().replace('_', ' ') }}"

    def ready(self):
        # setup sentry logging
        sentry_sdk.init(
            dsn=settings.SENTRY_DSN,
            release=f"{{cookiecutter.project_name}}-api@{os.environ.get('release_version', 'unknown-version')}",
            send_default_pii=True,
            integrations=[DjangoIntegration()],
        )
