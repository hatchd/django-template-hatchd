import logging
import os

from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from {{cookiecutter.app_name}}.utils.healthcheck import Healthcheck
from users.models import User

logger = logging.getLogger(__name__)


class ELBHealthcheckView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        """
        Healthcheck endpoint for the AWS Elastic Load Balancer, to ensure an instance is healthy and can serve requests
        """

        return Response(data={"users": User.objects.count()}, status=status.HTTP_200_OK)


@Healthcheck.register("example")
def example_healthcheck():
    return Healthcheck.STATUS_OK, ""


class NagiosHealthcheckView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        """
        Returns detailed information on the status of a set of registered checks
        """
        health_status = Healthcheck.STATUS_OK
        check_results = {}

        for check_name, check_func in Healthcheck.get_checks():
            try:
                check_status, check_data = check_func()
                check_results[check_name] = {"status": check_status, "data": check_data}
            except Exception as e:
                logger.exception(f"An error occurred getting the health of {check_name}.")
                check_status = "error"
                check_data = str(e)
                check_results[check_name] = {"status": check_status, "data": check_data}

            # update the check status
            if check_status != "ok":
                if check_status == "warning" and health_status != "error":
                    health_status = check_status
                elif check_status == "error":
                    health_status = check_status

        return Response(
            data={
                "status": health_status,
                "checks": check_results,
                "release": (
                    f"Release {os.environ.get('release_version', 'unknown')} "
                    f"built at {os.environ.get('build_date', 'unknown')}"
                ),
            },
            status=status.HTTP_200_OK,
        )
