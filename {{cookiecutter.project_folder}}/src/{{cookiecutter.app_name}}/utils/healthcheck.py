class Healthcheck:
    """Class to register and retrieve a set of health check functions

    usage:
    @Healthcheck.register("my_check")
    def my_check():
        ...
    """

    STATUS_OK = "ok"
    STATUS_WARNING = "warning"
    STATUS_ERROR = "error"

    _checks = {}

    @classmethod
    def register(cls, check_name):
        def wraps(func):
            if check_name in cls._checks:
                raise ValueError(f"{check_name} is already registered with {cls._checks[check_name]}")
            cls._checks[check_name] = func
            return func

        return wraps

    @classmethod
    def get_checks(cls):
        return cls._checks.items()
