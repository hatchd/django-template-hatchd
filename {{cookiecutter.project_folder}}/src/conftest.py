import pytest


@pytest.fixture(scope="function")
def user(db, django_user_model):
    return django_user_model.objects.create_user(
        email="test@example.com",
        name="Mr Test",
        password="Pass123#",
    )


@pytest.fixture(scope="function")
def staff_user(db, django_user_model):
    return django_user_model.objects._create_user(
        email="staff@example.com",
        name="Mr Staff",
        password="Pass123#",
        is_active=True,
        is_staff=True,
        is_superuser=False,
    )


@pytest.fixture(scope="function")
def admin_user(db, django_user_model):
    return django_user_model.objects.create_superuser(
        email="admin@example.com",
        name="Mr Admin",
        password="Pass123#",
    )
