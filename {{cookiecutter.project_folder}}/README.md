# {{cookiecutter.project_title}} API
<!-- TODO brief description of what the project does. Any cool illustrations, ASCII art and screen cap could also go here. -->
Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin grog yardarm hempen halter furl. Swab barque interloper chantey doubloon starboard grog black jack gangway rutters.
```
                 ;i.
                  M$L                    .;i.
                  M$Y;                .;iii;;.
                 ;$YY$i._           .iiii;;;;;
                .iiiYYYYYYiiiii;;;;i;iii;; ;;;
              .;iYYYYYYiiiiiiYYYiiiiiii;;  ;;;
           .YYYY$$$$YYYYYYYYYYYYYYYYiii;; ;;;;
         .YYY$$$$$$YYYYYY$$$$iiiY$$$$$$$ii;;;;
        :YYYF`,  TYYYYY$$$$$YYYYYYYi$$$$$iiiii;
        Y$MM: \  :YYYY$$P"````"T$YYMMMMMMMMiiYY.
     `.;$$M$$b.,dYY$$Yi; .(     .YYMMM$$$MMMMYY
   .._$MMMMM$!YYYYYYYYYi;.`"  .;iiMMM$MMMMMMMYY
    ._$MMMP` ```""4$$$$$iiiiiiii$MMMMMMMMMMMMMY;
     MMMM$:       :$$$$$$$MMMMMMMMMMM$$MMMMMMMYYL
    :MMMM$$.    .;PPb$$$$MMMMMMMMMM$$$$MMMMMMiYYU:
     iMM$$;;: ;;;;i$$$$$$$MMMMM$$$$MMMMMMMMMMYYYYY
     `$$$$i .. ``:iiii!*"``.$$$$$$$$$MMMMMMM$YiYYY
      :Y$$iii;;;.. ` ..;;i$$$$$$$$$MMMMMM$$YYYYiYY:
       :$$$$$iiiiiii$$$$$$$$$$$MMMMMMMMMMYYYYiiYYYY.
        `$$$$$$$$$$$$$$$$$$$$MMMMMMMM$YYYYYiiiYYYYYY
         YY$$$$$$$$$$$$$$$$MMMMMMM$$YYYiiiiiiYYYYYYY
        :YYYYYY$$$$$$$$$$$$$$$$$$YYYYYYYiiiiYYYYYYi'
```

<p align="right">(<a href="#top">back to top</a>)</p>


## Technologies
This project was build using [Django](https://www.djangoproject.com/) and uses following tech stacks:
<!-- https://www.tablesgenerator.com/markdown_tables generate a table for the tech stack-->
<!-- The following list is basically a copy from the `pyproject.toml`. -->

|                                                                                            | Usage                        | Version                         |
| ------------------------------------------------------------------------------------------ | ---------------------------- | ------------------------------- |
| [python]( https://www.python.org/ )                                                        |                              | {{cookiecutter.python_version}} |
| [Django]( https://www.djangoproject.com/ )                                                 |                              | {{cookiecutter.django_version}} |
| [djangorestframework]( https://www.django-rest-framework.org/ )                            | REST framework               | 3.13.1                          |
| [graypy](https://github.com/severb/graypy)                                                 | Logging handlers             | 2.1.0                           |
| [psycopg2](https://psycopg.org/)                                                           | PostgreSQL Database          | 2.9.3                           |
| [sentry-sdk](https://github.com/getsentry/sentry-python)                                   | Sentry                       | 0.20.3                          |
| [whitenoise](https://whitenoise.evans.io/)                                                 | Static file serving for WSGI | 5.3.0                           |
| [argon2-cffi](https://github.com/hynek/argon2-cffi)                                        | Password hashing             | 20.1.0                          |
| [PyJWT](https://github.com/jpadilla/pyjwt)                                                 | JSON Web Token               | 2.4.0                           |
| [djangorestframework-simplejwt](https://github.com/jazzband/djangorestframework-simplejwt) | JSON Web Token               | 5.2.0                           |
| [pytest-django](https://pytest-django.readthedocs.io/)                                     | Testing                      | 4.5.2                           |
| [drf-spectacular](https://github.com/tfranzel/drf-spectacular)                             | OpenAPI schema generation    | 0.21.2                          |

<p align="right">(<a href="#top">back to top</a>)</p>


## Table of Contents

<!-- Use extensions to generate ToC (https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) -->
<!-- Follow the steps here to make sure it will generate a README that can be navigate within Bitbucket-->
<!-- To generate ToC : Shift + CMD + P + "Markdown All in One: Create Table of Contents" -->
<!-- To update ToC : Shift + CMD + P + "Markdown All in One: Update Table of Contents" -->
- [{{cookiecutter.project_title}} API](#markdown-header-cookiecutterproject_title-api)
    - [Technologies](#markdown-header-technologies)
    - [Table of Contents](#markdown-header-table-of-contents)
    - [Usage](#markdown-header-usage)
        - [API Example Output](#markdown-header-api-example-output)
        - [Available API](#markdown-header-available-api)
        - [Glossary](#markdown-header-glossary)
    - [Installing Prerequisites](#markdown-header-installing-prerequisites)
    - [Setting up the Project](#markdown-header-setting-up-the-project)
        - [Dependencies](#markdown-header-dependencies)
        - [Setup](#markdown-header-setup)
        - [Running the API](#markdown-header-running-the-api)
        - [Running the tests](#markdown-header-running-the-tests)
    - [Running using Docker](#markdown-header-running-using-docker)
<p align="right">(<a href="#top">back to top</a>)</p>


## Usage
<!-- THIS SECTION CONTAINS DUMMY DATA AS AN EXAMPLE AND SHOULD BE EDITED ACCORDINGLY. -->
### API Example Output
<!-- TODO API example output -->
```
import foobar

# returns 'words'
foobar.pluralize('word')

# returns 'geese'
foobar.pluralize('goose')

# returns 'phenomenon'
foobar.singularize('phenomena')
```
<p align="right">(<a href="#top">back to top</a>)</p>


### Available API
<!-- TODO Available API calls -->
#### `foobar.pluralize('goose')`<!-- omit in toc -->
Pluralize the word provided.
#### `foobar.singularize('phenomena')`<!-- omit in toc -->
Singularize the word provided.
<p align="right">(<a href="#top">back to top</a>)</p>


### Glossary
<!-- TODO List out what some variables means in the codebase. -->
<!-- TODO Glossary list -->
#### `word`<!-- omit in toc -->
The word user provided to the api.
#### `final_form`<!-- omit in toc -->
The tranformed form of the work user requested.
<p align="right">(<a href="#top">back to top</a>)</p>


## Installing Prerequisites
<!-- TODO This should list out all the step-by-step command line prompt needed to install all the prerequisites the project needed for it the run normally.  -->
Poetry is used for dependency management along side pyenv for managing the python version.

[Install poetry](https://python-poetry.org/docs/#installation) (Only use the official script or pipx)

    curl -sSL https://install.python-poetry.org | python3 -
OR

    pipx install poetry

Check the correct Poetry version is installed

    poetry -V
    > Poetry (version {{cookiecutter.poetry_version}})

[Install pyenv](https://github.com/pyenv/pyenv#homebrew-on-macos)

    brew install pyenv
<p align="right">(<a href="#top">back to top</a>)</p>


---

## Setting up the Project
<!-- TODO This section list out all the necesary step to build the project and run locally. -->

### Dependencies
<!-- TODO Step-by-step prompts to install all the dependencies for the API to setup locally. -->

Pyenv should be activated first, to ensure you have the right version of python (according to the `.python-version` file)

    pyenv install --skip-existing

Poetry can then handle installing all the development dependencies

    poetry install
<p align="right">(<a href="#top">back to top</a>)</p>


### Setup
<!-- TODO Step-by-step prompts to setup the API for running locally. -->

Run the migrations

    poetry run python src/manage.py migrate

You can also restore an environment database

    restoretool restore [staging|production]
<p align="right">(<a href="#top">back to top</a>)</p>


### Running the API
<!-- TODO Step-by-step prompts to run the API locally and to be used. -->
    poetry run python src/manage.py runserver
<p align="right">(<a href="#top">back to top</a>)</p>

### Running the tests
<!-- TODO Step-by-step prompts to run the tests locally. -->
    # Running full test suite
    poetry run pytest

    # Running specific test file
    poetry run pytest {{cookiecutter.app_name}}/tests/test_foobar.py

    # Running specific named test
    poetry run pytest -k test_foobar
<p align="right">(<a href="#top">back to top</a>)</p>


## Running using Docker
<!-- TODO Docker stuff should be here. -->
To avoid having to install various tools, you can simply run the API within docker:

    tools/build-docker.sh
    tools/run-docker.sh
<p align="right">(<a href="#top">back to top</a>)</p>
