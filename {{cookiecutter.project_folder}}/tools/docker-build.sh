#!/bin/bash

# Die on error
set -e

docker build -f docker/Dockerfile -t {{cookiecutter.project_folder}}:latest --build-arg release_version=local_dev --build-arg build_date="$(TZ=":Australia/Perth" date)" .
