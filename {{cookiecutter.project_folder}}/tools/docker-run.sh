#!/bin/bash

# Die on error
set -e

docker run -e DJANGO_SETTINGS_MODULE={{cookiecutter.project_name}}.settings.docker -p 8080:8080 {{cookiecutter.project_folder}}:latest
