#!/bin/bash
set -e

# Handle different commands
case $1 in
    *)
        # Create static assets
        echo "Running django collectstatic in the background ..."
        python src/manage.py collectstatic --no-input &

        # Run the migrations
        echo "Running django migrate ..."
        python src/manage.py migrate
        echo "Done"

        # Start the api
        echo "Starting gunicorn on http://127.0.0.1:8080/ ..."
        cd src/
        exec gunicorn {{cookiecutter.project_name}}.asgi:application \
            --bind 0.0.0.0:8080 \
            --access-logfile - \
            --workers 4 \
            --worker-class uvicorn.workers.UvicornWorker
        ;;
esac
